
// 1
var promise = sqrt(4);

// 2
var promise = sqrt(-1);

// 3
var promise = sqrt(9)
    .then(result => {
        return result + 1;
    });

// 4
var promise = sqrt(9)
    .then(result => result + 1)
    .then(result => result + 2);

// 5
var promise = sqrt(16)
    .then(result => {
        return sqrt(result);
    });

// 6
var promise = sqrt(16)
    .then(result => sqrt(result))
    .then(result => result + 1);

// 7
var promise = sqrt(4)
    .then(result => {
        console.log(result);
    });

// 8
var promise = sqrt(4)
    .then(result => {
        throw 'hello';
    })
    .then(result => console.log(result));

// 9
var promise = sqrt(4)
    .then(() => Math.hello())
    .then(result => console.log(result));

// 10
var promise = sqrt(4)
    .then(() => {
        throw 'error'
    }).catch(error => {
        console.log('logging: ' + error);
    });

// 11
var promise = sqrt(4)
    .then(() => {
        throw 'error'
    }).catch(error => {
        console.log('logging: ' + error);
        throw error;
    });
