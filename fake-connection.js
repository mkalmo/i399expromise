
var database;

var p = connect('mongodb://...').then(db => {
    database = db;

    return db.insert('abc');
});

p = p.then(() => {
    closeDb(database);

    console.log('All done');
});

p.catch(error => {
    closeDb(database);

    console.log('Error: ' + error);
});

function connect(url) {

    var fakeDb = {
        insert: function (what) {
            // return Promise.reject('problem with insert');

            return Promise.resolve()
                .then(() => console.log(`inserted '${what}' to database`));
        },

        close: () => console.log('closing db')
    };

    // return Promise.reject("can't connect");

    return Promise.resolve()
        .then(() => console.log('connected to ' + url))
        .then(() => fakeDb);
}

function closeDb(database) {
    if (database) {
        database.close();
    } else {
        console.log('nothing to close db is undefined');
    }
}