var LoggingPromise = require('./logging-promise');

// test case goes between the lines
// -------------------------------------------------------------

var promise = sqrt(4);

// -------------------------------------------------------------

promise.then(result => console.log('Final result: ' + result))
       .catch(error => console.log('Final error: ' + error));

function sqrt(arg) {
    console.log('calculate sqrt(' + arg + ') called');

    var realPromise = parseInt(arg) >= 0
        ? Promise.resolve(Math.sqrt(arg))
        : Promise.reject('bad argument');

    return new LoggingPromise(realPromise);
}
