const THEN_COUNTER = 'thenCounter';
const CATCH_COUNTER = 'catchCounter';

function LoggingPromise(realPromise) {

    this.then = wrapWithLogger('then cb', THEN_COUNTER, realPromise.then);
    this.catch = wrapWithLogger('catch cb', CATCH_COUNTER, realPromise.catch);

    function wrapWithLogger(prefix, counterName, func) {

        return function then(cb) {

            // do not log internal stuff
            if (/\[native code\]/.test(cb.toString())) {
                realPromise.then(cb);
                return;
            }

            var counter = LoggingPromise.prototype[counterName]++;

            console.log(`${prefix} ${counter} added: ${cb.toString()}`);

            realPromise = func.call(realPromise, input => {

                console.log(`${prefix} ${counter} executed`);
                console.log(`  ${prefix} ${counter} input: ${input}`);

                try {
                    var result = cb(input);
                } catch (error) {
                    console.log(`  ${prefix} ${counter} threw: ${error}`);
                    return Promise.reject(error);
                }

                var resultValue = typeof result === 'object' ?
                    `Object of type: ${result.constructor.name}` : result;

                console.log(`  ${prefix} ${counter} returned: ${resultValue}`);

                return result;
            });

            return new LoggingPromise(realPromise);
        }


    }

}

LoggingPromise.prototype[THEN_COUNTER] = 1;
LoggingPromise.prototype[CATCH_COUNTER] = 1;

module.exports = LoggingPromise;